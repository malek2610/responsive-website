# Bootstrap 5 Responsive Website

## Description

This is a simple responsive website developed under `Bootstrap 5`.

## Lessons Learnt

### 1. Bootstrap 5 Framework

- Migration of `.ml-*` and `.mr-*` to `.ms-*` and `.me-*`
- For local `css` and `js` files, use:

  - `bootstrap.min.css`
  - `bootstrap.bundle.min.js`

- Carousel: Consistent Height Carousels with CSS Gradients and Object Fit
  ```css
  #carouselExampleIndicators .carousel-item img {
    object-fit: cover;
    object-position: center;
    height: 60vh;
    overflow: hidden;
  }
  ```

## Links

- [Bootstrap](https://getbootstrap.com/)
- [Hacking the Bootstrap 5 Carousel](https://www.youtube.com/watch?v=KHF6nysy0-c)
